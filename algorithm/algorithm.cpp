#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <string>
#include <vector>

using namespace std;

int main() {
  vector<int> ints{1,2,3,4,5,6,5,4,3,2,1};

  const auto three_count = count(begin(ints), end(ints), 3);

  cout << three_count << endl;

  const auto odds_count = count_if(
    begin(ints),
    end(ints),
    [](auto val) {return val % 2 != 0;}
  );

  cout << odds_count << endl;

  const auto has_number_22 = any_of(
    begin(ints),
    end(ints),
    [](auto val) {return val == 22;}
  );

  cout << "has number 22? " << (has_number_22 ? "yes" : "no") << endl;

  const auto missing_99 = none_of(
    begin(ints),
    end(ints),
    [](auto val) {return val == 99;}
  );

  cout << "missing number 99? " << (missing_99 ? "yes" : "no") << endl;

  const string name{"Jane Doe"};

  const auto letter_e = find(
    begin(name),
    end(name),
    'e'
  );

  cout << "has letter e? " << (letter_e == end(name) ? "no" : "yes") << endl;

  const vector<int> primes {3, 5};

  const auto first_prime = find_first_of(
    begin(ints),
    end(ints),
    begin(primes),
    end(primes)
  );

  cout << "should be 3: " << *first_prime << endl;

  const vector<int> hidden_pair = {1, 2, 3, 4, 4, 5, 6, 5, 4, 3, 2, 1};

  const auto first_pair = adjacent_find(begin(hidden_pair), end(hidden_pair));

  cout << "first element of hidden pair is: " << *first_pair << endl;

  vector<int> tasks(6);
  iota(begin(tasks), end(tasks), 65);

  std::copy( begin(tasks), end(tasks), std::ostreambuf_iterator<char>(std::cout) );

  cout << endl;

  auto B = find(begin(tasks), end(tasks), 66);
  auto E = find(begin(tasks), end(tasks), 69);

  rotate(B, E, E + 1);

  std::copy( begin(tasks), end(tasks), std::ostreambuf_iterator<char>(std::cout) );
}
