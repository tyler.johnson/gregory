set -e
set -o pipefail
mkdir -p build
cd build
cmake ..
make 2>&1 |head -n 30
