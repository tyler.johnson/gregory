#include "hello.h"
#include <iostream>

int main() {
  std::cout << "Name, please." << std::endl;
  std::string name;
  name = "No TTY!";
  std::cin >> name;
  std::cout << "Hello, " << name << std::endl;
}
