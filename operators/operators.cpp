int main() {
  int i = 0;
  i += 1;
  i *= 3;
  i -= 2;
  i /= 4;

  int j = i++;
  j += 1;

  int k = ++i;

  return j + i + k;
}
