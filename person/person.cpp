#include "person.h"
#include <string>
#include <utility>

Person::Person(std::string first, std::string last) :
  firstName(std::move(first)),
  lastName(std::move(last))
{}

std::string Person::getFirst() const {
  return firstName;
}

std::string Person::getLast() const {
  return lastName;
}

Person Person::operator<<(int i) {
  return i % 2 == 0 ?
    Person(firstName, lastName) :
    Person(lastName, firstName);
}

Person Person::operator>>(int i) {
  return i % 2 == 0 ?
    Person(firstName, lastName) :
    Person(lastName, firstName);
}
