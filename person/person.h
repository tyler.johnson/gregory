#ifndef __PERSON_H__
#define __PERSON_H__
#include <string>

class Person {
  private:
    const std::string firstName;
    const std::string lastName;

  public:
    std::string getFirst() const;
    std::string getLast() const;
    Person(std::string first, std::string last);
    Person operator>>(int i);
    Person operator<<(int i);
};
#endif
