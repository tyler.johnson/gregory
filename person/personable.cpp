#include "person.h"
#include <iostream>

using std::cout;
using std::endl;

int main() {
  Person p1("first1", "last1");
  Person p2("first2", "last2");
  Person p3 = p2 << 1;

  cout << p1.getFirst() << " " << p1.getLast() << endl;
  cout << p2.getFirst() << " " << p2.getLast() << endl;
  cout << p3.getFirst() << " " << p3.getLast() << endl;
  cout << p2.getFirst() << " " << p2.getLast() << endl;
}
